﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class x1Controller : MonoBehaviour {

	public TextMesh x1spawner;
	
	private void OnEnable() {
		StartCoroutine(Vanish());
	}
	private void Update() {
		x1spawner.text = "x " + GameManager.instance.multiplier + " ";
	}

	IEnumerator Vanish() {
		yield return new WaitForSeconds(.4f);
		gameObject.SetActive(false);
	}

}
		

