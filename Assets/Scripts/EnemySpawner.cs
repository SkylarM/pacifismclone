﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public static EnemySpawner instance;

    public GameObject enemyPrefab;
    public int poolSize = 200;
    private GameObject[] enemyPool;
    private int currentIndex = 0;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
 
    }

    void Start () {
        enemyPool = new GameObject[poolSize];
        for (int i = 0; i < poolSize; i++) {
            GameObject enemy = Instantiate(enemyPrefab);
            enemy.SetActive(false);
            enemyPool[i] = enemy;
        }
	}

    public void Spawn (Vector3 pos) {
        GameObject enemy = enemyPool[currentIndex];
        enemy.transform.position = pos;
        enemy.SetActive(true);
        currentIndex++;
        currentIndex %= poolSize;
    }

    
}
