﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionSpawner : MonoBehaviour {

	public static ExplosionSpawner instance;

    public GameObject ExplosionPrefab;
    public int poolSize = 20;
    private GameObject[] ExplosionPool;

    private int currentIndex = 0;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    void Start () {
        ExplosionPool = new GameObject[poolSize];
        for (int i = 0; i < poolSize; i++) {
            GameObject explosion = Instantiate(ExplosionPrefab);
            explosion.SetActive(false);
            ExplosionPool[i] = explosion;
        }
	}

    public void Spawn (Vector3 pos) {
        GameObject g = ExplosionPool[currentIndex];
        g.transform.position = pos;
        currentIndex++;
        currentIndex %= poolSize;

		ExplosionController exp = g.GetComponent<ExplosionController>();
		exp.Explode();
    }
}

