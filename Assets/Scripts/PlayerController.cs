﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public GameManager GM;

    public Text Score;
    public static PlayerController instance;
    public float speed = 10;    
    public ParticleSystem gateDieParticles;
    public ParticleSystem playerDieParticles;
    public float turnSpeed = 10;
    public AudioSource gateDieSound;
    public AudioSource playerDieSound;
    public AudioSource pickup;

    Rigidbody body;

    void Awake () {
        if (instance == null) {
            instance = this;
        }
    }
    

	void Start () {
        body = GetComponent<Rigidbody>();
        GM = GameManager.instance;
    }
	
	void Update () {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        Vector3 dir = new Vector3(x, 0, z).normalized;
        body.velocity = dir * speed;


        if(dir.magnitude > 0.1f) {
            Vector3 cross = Vector3.Cross(transform.right, dir);
            transform.Rotate(Vector3.up * cross.y * turnSpeed * Time.deltaTime);

        }
        

        
    }

    void OnTriggerEnter (Collider c) {
        GameManager.instance.score++;
        if (c.gameObject.CompareTag("Enemy") || c.gameObject.CompareTag("bomb")) { 
            gameObject.SetActive(false);
            playerDieParticles.transform.position = transform.position;
            playerDieParticles.Play();
            playerDieSound.Play();
            GM.gameOver = true;
        }
        if(c.gameObject.GetComponent<GateController>() != null) {
            GM.score++;
            c.gameObject.SetActive(false);
            GM.scoreText.text = GM.score + "pts";
            gateDieParticles.transform.position = transform.position;
            gateDieParticles.Play();
            gateDieSound.pitch = Random.Range(0.02f, 1f);
            gateDieSound.Play();
        }
          if(c.gameObject.GetComponent<MultiplierController>() != null) {
            GM.multiplier++;
            pickup.pitch = Random.Range(0.02f, 1f);
            pickup.Play();
            c.gameObject.SetActive(false);
            GM.multiplierText.text = "x" + GM.multiplier;
    }
    
}
    }


    