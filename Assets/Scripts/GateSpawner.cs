﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateSpawner : MonoBehaviour {

    public static GateSpawner instance;

    public GameObject gatePrefab;
    public int poolSize = 12;
    private GameObject[] gatePool;
    private int currentIndex = 0;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    void Start () {
        gatePool = new GameObject[poolSize];
        for (int i = 0; i < poolSize; i++) {
            GameObject gate = Instantiate(gatePrefab);
            gate.SetActive(false);
            gatePool[i] = gate;
        }
	}

    public void Spawn (Vector3 pos) {
        GameObject gate = gatePool[currentIndex];
        gate.transform.position = pos;
        gate.SetActive(true);
        currentIndex++;
        currentIndex %= poolSize;
    }
}

