﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplierController : MonoBehaviour {
 
  public float speed = 20;
	public float lifeTime = 5;
	public float warningTime = 0.5f;
	public TextMesh x1;

	Rigidbody body;
	void Start () {
		body = GetComponent<Rigidbody>();
	}

	void OnEnable () {
        Renderer r = GetComponentInChildren<Renderer>();
		r.enabled = true;
        StartCoroutine(SelfDestruct());
	}

	IEnumerator SelfDestruct () {
		yield return new WaitForSeconds(lifeTime - warningTime);
		Renderer r = GetComponentInChildren<Renderer>();
		for (float t = 0; t < warningTime; t += Time.deltaTime) {
			r.enabled = !r.enabled;
			yield return new WaitForEndOfFrame();
		}
		gameObject.SetActive(false);
	}
	
	void OnTriggerEnter (Collider c) {
		if (c.CompareTag("Player")) {
			StartCoroutine(GoToPlayer());
			x1Spawner.instance.Spawn(transform.position);
			x1.text = "x1";
		    gameObject.SetActive(false);
		}
	}
	
    void OnTriggerExit(Collider c) {
        if (c.CompareTag("Player")) {
            StopCoroutine("GoToPlayer");
        }
    }

	IEnumerator GoToPlayer() {
		while (enabled) {
			Vector3 dir = PlayerController.instance.transform.position - transform.position;
			dir = dir.normalized;
			body.velocity = dir * speed;
			yield return new WaitForEndOfFrame();
		}
	}

	void OnCollisionEnter (Collision c) {
        if (c.gameObject.CompareTag("Player")) {
			GameManager.instance.PickupMultiplier();
			gameObject.SetActive(false);
		}

		if (c.gameObject.tag==("Explosion")) {
			gameObject.SetActive(false);
			x1Spawner.instance.Spawn(transform.position);
			

		}
	}
}
