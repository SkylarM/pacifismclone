﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {


    public static GameManager register;
    public GameObject gameOverScreen;
    public GameObject RightSide;
    public GameObject LeftSide;
    public Animation anim;
    public Text scoreText;

    public Text multiplierText;
    public Text highScoreText;
    public int score = 0;

    public static GameManager instance;
    bool gameInPlay;
    public bool gameOver;
    public int multiplier = 1;


    void Awake() {
        if (instance == null) {
            Debug.Log("GameManager.instance == " + name);
            instance = this;
        }
        else {
            Destroy(this.gameObject);
        }
      }

    IEnumerator Start() {
        int highScore = PlayerPrefs.GetInt("HighScore", 0);
        highScoreText.text = "" + highScore;
        while (enabled) { 
            yield return new WaitForSeconds(4);
            for (int i = 0; i < 10; i++) {
                int nextEnemyPoint = Random.Range(0, 4);
                Vector3[] points = new Vector3[] {
                    new Vector3(25, 0, 25),
                    new Vector3(25, 0, -25),
                    new Vector3(-25, 0, 25),
                    new Vector3(-25, 0, -25)
                };
                EnemySpawner.instance.Spawn(points[nextEnemyPoint]);
            }
            AudioManager.instance.PlaySFX("EnemySpawnSound");

            yield return new WaitForSeconds(.3f);
            GateSpawner.instance.Spawn(new Vector3(Random.Range(-19f, 19f), 0, Random.Range(-19f, 19f)));

            if (gameInPlay) {
                gameOverScreen.SetActive(false);
            }
            else if (gameOver) {
                gameOverScreen.SetActive(true);
               LeftSide.SetActive(false);
               RightSide.SetActive(false);
            }
        }
    }
    void Update () {
        scoreText.text = score + "pts";
		int highScore = PlayerPrefs.GetInt("HighScore", 0);
		if (score > highScore) {
			PlayerPrefs.SetInt("HighScore", score);
			PlayerPrefs.Save();
			highScoreText.text = "" + highScore;
        }
    }

    public void KilledEnemy () {
		if (gameOver) return;
		score += 10 * multiplier;
        scoreText.text = score + " pts";
	}
      
    public void AddToMultiplier()
    {
        multiplier += 1;
        multiplierText.text = "x " + multiplier;
    }
    public void PickupMultiplier () {
        if (gameOver) return;
		multiplier++;
		multiplierText.text = "x" + multiplier;
	}
}
