﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplierSpawner : MonoBehaviour {

    public static MultiplierSpawner instance;

    public GameObject multiplierPrefab;
    public int poolSize = 25;
    private GameObject[] multiplierPool;
    private int currentIndex = 0;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    void Start () {
        multiplierPool = new GameObject[poolSize];
        for (int i = 0; i < poolSize; i++) {
            GameObject multiplier = Instantiate(multiplierPrefab);
            multiplier.SetActive(false);
            multiplierPool[i] = multiplier;
        }
	}

    public void Spawn (Vector3 pos) {
        GameObject multiplier = multiplierPool[currentIndex];
        multiplier.transform.position = pos;
        multiplier.SetActive(true);
        currentIndex++;
        currentIndex %= poolSize;
    }
}

