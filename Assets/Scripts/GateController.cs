﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateController : MonoBehaviour {

	public Transform[] bombs;

	void OnEnable () {
		transform.eulerAngles = Vector3.up * Random.value * 360;
		
	}
	void OnTriggerEnter (Collider c) {
		if (c.CompareTag("Player")) {
			for (int i = 0; i < bombs.Length; i++) {
				ExplosionSpawner.instance.Spawn(bombs[i].position);

			}
		}
	}
}

