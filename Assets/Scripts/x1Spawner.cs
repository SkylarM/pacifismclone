﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class x1Spawner : MonoBehaviour {

    public static x1Spawner instance;
    public TextMesh x1text;

    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    public GameObject x1prefab;
    public int poolSize = 2;

    GameObject[] numPool;
    int currentIndex = 0;

    private void Start() {
        numPool = new GameObject[poolSize];
        for( int i = 0; i < poolSize; i++) {
            GameObject g = Instantiate(x1prefab);
            g.SetActive(false);
            numPool[i] = g;
        }
    }

    public void Spawn(Vector3 pos) {
        GameObject q = numPool[currentIndex];
        q.transform.position = pos;
        q.SetActive(true);
        currentIndex++;
        currentIndex %= poolSize;
    }
}
