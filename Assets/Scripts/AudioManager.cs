﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	public static AudioManager instance;

	public AudioClip[] enemySpawnClips;
	
	public int maxSFXSources = 10;
	private AudioSource[] enemySpawnPool;
	private int currentSFX;


	void Awake () {
		if (instance == null) {
			instance = this;
		}
	}

	void Start () {
		enemySpawnPool = new AudioSource[maxSFXSources];
		for (int i = 0; i < maxSFXSources; i++) {
			GameObject g = new GameObject("sfx"+i);
			AudioSource sfx = g.AddComponent<AudioSource>();
			sfx.playOnAwake = false;
			enemySpawnPool[i] = sfx;

		}
	}

	public void PlaySFX (string clipName) {
		 for (int i =0; i < enemySpawnClips.Length; i++) {
			 if (clipName == enemySpawnClips[i].name) {
				 AudioSource enemySpawn = enemySpawnPool[currentSFX];
				 enemySpawn.clip = enemySpawnClips[i];
				 enemySpawn.pitch = Random.Range(1.5f, 5);
				 enemySpawn.Play();
				 currentSFX++;
				 currentSFX %= maxSFXSources;
				 break;
			 }
		 }
	}
}
