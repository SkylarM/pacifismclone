﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

	public float speed = 3;

	Rigidbody body;
	void Start () {
		body = GetComponent<Rigidbody>();
	}
	
	void Update () {
		Vector3 dir = PlayerController.instance.transform.position - transform.position;
		dir = dir.normalized;
		body.velocity = dir * speed;
		transform.forward = dir;
	}

	void OnTriggerEnter (Collider c) {
		if (c.CompareTag("Explosion")) {
			gameObject.SetActive(false);
			MultiplierSpawner.instance.Spawn(transform.position);
			GameManager.instance.KilledEnemy();
	}
		}

}
