﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionController : MonoBehaviour {

	public float expandDuration = 0.4f;
	public float expandStartSize = 1;
	public float expandEndSize = 10;

	void Awake () {
		transform.gameObject.SetActive(false);
	}

	public void Explode () {
		transform.gameObject.SetActive(false);
		gameObject.SetActive(true);
		StartCoroutine("ExplosionCoroutine");
	}

	IEnumerator ExplosionCoroutine () {
		transform.localScale = Vector3.one * expandStartSize;
		transform.gameObject.SetActive(true);

		WaitForEndOfFrame wait = new WaitForEndOfFrame();
		for (float t = 0; t < expandDuration; t += Time.deltaTime) {
			float frac = t / expandDuration;
			transform.localScale = Vector3.one * Mathf.Lerp(expandStartSize, expandEndSize, frac);
			yield return wait;
		}

		transform.localScale = Vector3.one * expandEndSize;
		transform.gameObject.SetActive(false);
		gameObject.SetActive(false);
	}
}
