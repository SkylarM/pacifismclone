﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineTest : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        StartCoroutine(LerpLightColor());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator LerpLightColor()
    {
        Color a = Color.red;
        Color b = Color.blue;
        float duration = 0.5f;
        while (enabled)
        {
            for (float t = 0; t < duration; t += Time.deltaTime)
            {
                float frac = t / duration;
                GetComponent<Light>().color = Color.Lerp(a, b, frac);
                yield return new WaitForEndOfFrame();
            }

            for (float t = 0; t < duration; t += Time.deltaTime)
            {
                float frac = t / duration;
                GetComponent<Light>().color = Color.Lerp(a, b, frac);
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
